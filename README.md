# FBGaming-SlimChat Tampermonkey

CSS changes for the Facebook gaming chat, to cut down on dead space and fit more messages on one screen.


## Description
This is a simple script for Tampermonkey, which aims to improve the Facebook Livestream chat, by reducing the amount of dead space.
Current modifications / features:

    - a smaller and dark scrollbar
    - deletes the button to the right of each message (used to hide a message)
    - deletes the share button (you can simply share the link anyways - why is this here FB?)
    - option to hide user avatars (disabled by default -> uncomment the line)
    
    - general CSS changes, making corners sharper and reducing the space between messages
    - reactions are moved to the top right of the message box
    - badges infront of the username (like on twitch)
    - bold font for usernames
    - hides the "Like" and "Reply" buttons and reveals them again when hobering on the message with the mouse
    - hides the "Replying to username" text above replies - the original message of the thread will still be displayed

Comparison between with script and default chat layout:

![example](https://i.imgur.com/mVvfvI3.png)

## Installation

1. Install the [Tampermonkey addon](https://www.tampermonkey.net/) for your browser if you haven't already
2. Create a new Userscript with Tampermonkey
3. Copy and paste the [script](https://gitlab.com/RaptorPort/fbgaming-slimchat-tampermonkey/-/blob/main/FBGamingSlimChat.js)
4. Save (press Ctrl+S) and you are done! 

Optional: If you want automatic updates for the script, click on settings -> Check for updates -> Save



## Contributing
If you have any requests or suggestions let me know, I'm also open for any help ;)

## License
MIT-License

## Project status
This is a small sideproject for me, to improve the chat experience. It doesn't aim at completly replacing the current FB-Chat. It is just a modification.

